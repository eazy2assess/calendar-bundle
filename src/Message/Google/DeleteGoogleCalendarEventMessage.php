<?php

namespace Eazy\Bundle\EazyCalendar\Message\Google;

use Eazy\Bundle\EazyCalendar\Message\CalendarMessage;
use Eazy\Bundle\EazyCalendar\Model\CredentialsInterface;

class DeleteGoogleCalendarEventMessage implements CalendarMessage
{
    private $id;
    
    private CredentialsInterface $credentials; 
    
    public function __construct($id, CredentialsInterface $credentials)
    {
        $this->id = $id;
        $this->credentials = $credentials;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCredentials(): CredentialsInterface
    {
        return $this->credentials;
    }
    
}