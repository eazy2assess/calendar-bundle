<?php

namespace Eazy\Bundle\EazyCalendar\Message\Microsoft;

use Eazy\Bundle\EazyCalendar\Event\NewCalendarEvent;
use Eazy\Bundle\EazyCalendar\Message\CalendarMessage;

class AddNewMicrosoftCalendarEventMessage implements CalendarMessage
{
    private NewCalendarEvent $calendarEvent;

    public function __construct(NewCalendarEvent $calendarEvent)
    {
        $this->calendarEvent = $calendarEvent;
    }

    public function getCalendarEvent(): NewCalendarEvent
    {
        return $this->calendarEvent;
    }
}