<?php

namespace Eazy\Bundle\EazyCalendar\Exception\Google;

use Eazy\Bundle\EazyCalendar\Exception\EazyCalendarException;

class GoogleCalendarAuthorizationException extends EazyCalendarException
{

}