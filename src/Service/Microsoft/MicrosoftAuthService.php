<?php

namespace Eazy\Bundle\EazyCalendar\Service\Microsoft;

use Eazy\Bundle\EazyCalendar\Event\Microsoft\MicrosoftTokenRefreshedEvent;
use Eazy\Bundle\EazyCalendar\Model\Microsoft\MicrosoftCredential;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GenericProvider;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class MicrosoftAuthService
{
    private AbstractProvider $oauthClient;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(AbstractProvider $oauthClient, EventDispatcherInterface $eventDispatcher)
    {
        $this->oauthClient = $oauthClient;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getAuthUri(): string
    {
        return $this->oauthClient->getAuthorizationUrl();
    }

    public function getAuthorizationDataByCode(string $token): MicrosoftCredential
    {
        $timeNow = (new \DateTime())->getTimestamp();
        $accessToken = $this->oauthClient->getAccessToken('authorization_code', [
            'code' => $token
        ]);

        return new MicrosoftCredential(
            $timeNow,
            $accessToken->getToken(),
            $accessToken->getExpires(),
            $accessToken->getRefreshToken(),
            $accessToken->getValues()
        );
    }

    public function checkCredentials(MicrosoftCredential $credential): void
    {
        $timeNow = (new \DateTime())->getTimestamp();
        if ($credential->getExpires() > $timeNow) {
            return;
        }
        $accessToken = $this->oauthClient->getAccessToken('refresh_token', [
            'refresh_token' => $credential->getRefreshToken()
        ]);
        
        $credential->setAccessToken($accessToken->getToken());
        $credential->setCreatedAt($timeNow);
        $credential->setExpires($accessToken->getExpires());
        $credential->setRefreshToken($accessToken->getRefreshToken());
        $credential->setValues($accessToken->getValues());
        
        $this->eventDispatcher->dispatch(new MicrosoftTokenRefreshedEvent($credential));
    }
}