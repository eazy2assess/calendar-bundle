<?php

namespace Eazy\Bundle\EazyCalendar\Service\Google;

use Eazy\Bundle\EazyCalendar\Event\Google\GoogleTokenRefreshedEvent;
use Eazy\Bundle\EazyCalendar\Exception\Google\GoogleCalendarAuthorizationException;
use Eazy\Bundle\EazyCalendar\Model\Google\GoogleCredential;
use Google\Client;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class GoogleAuthService
{
    private Client $client;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(Client $client, EventDispatcherInterface $eventDispatcher)
    {
        $this->client = $client;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getAuthorizationDataByCode(string $code): GoogleCredential
    {
        $client = clone $this->client;
        $auth = $client->fetchAccessTokenWithAuthCode($code);
        $this->checkAuthResponse($auth);

        return new GoogleCredential(
            $auth['access_token'],
            $auth['expires_in'],
            $auth['created'],
            $auth['refresh_token'] ?? null
        );
    }

    public function getAuthUri(): string
    {
        $client = clone $this->client;

        return $client->createAuthUrl();
    }

    public function checkCredentials(GoogleCredential $credential): void
    {
        $client = clone $this->client;

        $client->setAccessToken([
            'access_token' => $credential->getToken(),
            'expires_in' => $credential->getExpiresIn(),
            'created' => $credential->getCreatedAt(),
            'refresh_token' => $credential->getRefreshToken(),
        ]);
        if (!$client->isAccessTokenExpired()) {
            return;
        }

        $auth = $client->fetchAccessTokenWithRefreshToken($credential->getRefreshToken());
        $this->checkAuthResponse($auth);

        $credential->setToken($auth['access_token']);
        $credential->setExpiresIn($auth['expires_in']);
        $credential->setCreatedAt($auth['created']);
        $credential->setRefreshToken($auth['refresh_token'] ?? null);

        $this->eventDispatcher->dispatch(new GoogleTokenRefreshedEvent($credential));
    }

    private function checkAuthResponse(array $auth): void
    {
        if (!isset($auth['error'])) {
            return;
        }

        throw new GoogleCalendarAuthorizationException(sprintf('Cannot get access token: %s', $auth['error_description'] ?? ''));
    }
}