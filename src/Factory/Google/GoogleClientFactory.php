<?php

namespace Eazy\Bundle\EazyCalendar\Factory\Google;

use Google\Client;

class GoogleClientFactory
{
    public static function create(array $config): Client
    {
        $client = new Client();

        $client->setAuthConfig([
            'client_id' => $config['clientId'],
            'project_id' => $config['projectId'],
            'auth_uri' => $config['authUri'],
            'token_uri' => $config['tokenUri'],
            'auth_provider_x509_cert_url' => $config['authProviderX509'],
            'client_secret' => $config['clientSecret'],
            'redirect_uris' => [$config['redirectUri']],
            'javascript_origins' => $config['javascriptOrigins'],
        ]);
        $client->addScope(\Google_Service_Calendar::CALENDAR);
        $client->setRedirectUri($config['redirectUri']);
        $client->setAccessType('offline');
        $client->setPrompt('select_account');
        $client->setApprovalPrompt('select_account');
        $client->setIncludeGrantedScopes(true);

        return $client;
    }
}