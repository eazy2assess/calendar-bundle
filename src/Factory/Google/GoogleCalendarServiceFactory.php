<?php

namespace Eazy\Bundle\EazyCalendar\Factory\Google;

use Eazy\Bundle\EazyCalendar\Model\Google\GoogleCredential;
use Eazy\Bundle\EazyCalendar\Service\Google\GoogleAuthService;
use Google\Client;
use Google\Service\Calendar;

class GoogleCalendarServiceFactory
{
    private Client $client;

    private GoogleAuthService $authService;

    public function __construct(Client $client, GoogleAuthService $authService)
    {
        $this->client = $client;
        $this->authService = $authService;
    }

    public function createCalendarService(GoogleCredential $credential): Calendar
    {
        $this->authService->checkCredentials($credential);
        $client = clone $this->client;
        $client->setAccessToken([
            'access_token' => $credential->getToken(),
            'expires_in' => $credential->getExpiresIn(),
            'created' => $credential->getCreatedAt(),
            'refresh_token' => $credential->getRefreshToken(),
        ]);

        return new Calendar($client);
    }
}