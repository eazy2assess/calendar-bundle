<?php

namespace Eazy\Bundle\EazyCalendar\Factory\Microsoft;

use Eazy\Bundle\EazyCalendar\Model\Microsoft\MicrosoftCredential;
use Eazy\Bundle\EazyCalendar\Service\Microsoft\MicrosoftAuthService;
use Microsoft\Graph\Graph;

class MicrosoftGraphFactory
{
    private MicrosoftAuthService $authService;

    public function __construct(MicrosoftAuthService $authService)
    {
        $this->authService = $authService;
    }
    
    public function create(MicrosoftCredential $credential): Graph
    {
        $this->authService->checkCredentials($credential);
        
        $graph = new Graph();
        $graph->setAccessToken($credential->getAccessToken());
        
        return $graph;
    }
}