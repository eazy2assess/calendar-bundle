<?php

namespace Eazy\Bundle\EazyCalendar\Factory\Microsoft;

use League\OAuth2\Client\Provider\GenericProvider;

class MicrosoftOAuthClientFactory
{
    public static function create(array $arguments): GenericProvider
    {
        return new GenericProvider($arguments);
    }
}
