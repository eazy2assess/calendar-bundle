<?php

namespace Eazy\Bundle\EazyCalendar\Manager\Google;

use Eazy\Bundle\EazyCalendar\Event\Google\GoogleCalendarEventCreatedEvent;
use Eazy\Bundle\EazyCalendar\Event\Google\GoogleCalendarEventDeletedEvent;
use Eazy\Bundle\EazyCalendar\Factory\Google\GoogleCalendarServiceFactory;
use Eazy\Bundle\EazyCalendar\Manager\CalendarEventManager;
use Eazy\Bundle\EazyCalendar\Model\CalendarEvent;
use Eazy\Bundle\EazyCalendar\Model\CalendarEventInterface;
use Eazy\Bundle\EazyCalendar\Model\CredentialsInterface;
use Eazy\Bundle\EazyCalendar\Model\ListQueryParams;
use Google\Service\Calendar;
use Google\Service\Calendar\Event;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class GoogleCalendarEventsManager implements CalendarEventManager
{
    private GoogleCalendarServiceFactory $calendarServiceFactory;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        GoogleCalendarServiceFactory $calendarServiceFactory,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->calendarServiceFactory = $calendarServiceFactory;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function addEventToCalendar(CalendarEventInterface $calendarEvent, CredentialsInterface $credentials): void
    {
        $calendarService = $this->createCalendarService($credentials);
        $attendees = [];
        foreach ($calendarEvent->getAttendees() as $attendee) {
            $attendees[]['email'] = $attendee;
        }
        $event = new Event([
            'summary' => $calendarEvent->getTitle(),
            'location' => $calendarEvent->getLocation(),
            'description' => $calendarEvent->getDescription(),
            'start' => [
                'dateTime' => $calendarEvent->getStartDate()->format(\DateTimeInterface::ATOM),
                'timeZone' => $calendarEvent->getStartDate()->getTimezone(),
            ],
            'end' => [
                'dateTime' => $calendarEvent->getEndDate()->format(\DateTimeInterface::ATOM),
                'timeZone' => $calendarEvent->getEndDate()->getTimezone(),
            ],
            'attendees' => $attendees,
            'reminders' => [
                'useDefault' => true,
                // TODO: reminders settings
//                'overrides' => [
//                    array('method' => 'email', 'minutes' => 24 * 60),
//                    array('method' => 'popup', 'minutes' => 10),
//                ],
            ],
        ]);

        $googleCreatedEvent = $calendarService->events->insert('primary', $event);

        $this->eventDispatcher->dispatch(new GoogleCalendarEventCreatedEvent($googleCreatedEvent, $calendarEvent->getAdditionalData()));
    }
    
    public function deleteEventFromCalendar($id, CredentialsInterface $credentials): void
    {
        $calendarService = $this->createCalendarService($credentials);
        $calendarService->events->delete('primary', $id);
        
        $this->eventDispatcher->dispatch(new GoogleCalendarEventDeletedEvent($id));
    }

    public function getEventsList(ListQueryParams $listQueryParams, CredentialsInterface $credentials): array
    {
        $calendarService = $this->createCalendarService($credentials);

        $queryParams = [];
        if ($listQueryParams->getFrom() instanceof \DateTime) {
            $queryParams['timeMin'] = $listQueryParams->getFrom()->format(DATE_RFC3339);
        }
        if ($listQueryParams->getTo() instanceof \DateTime) {
            $queryParams['timeMax'] = $listQueryParams->getTo()->format(DATE_RFC3339);
        }
        $queryParams['maxResults'] = $listQueryParams->getMaxResults();
        
        $items = [];
        $nextPage = true;
        while ($nextPage) {
            if (isset($response) && $response->getNextPageToken() !== null) {
                $queryParams['pageToken'] = $response->getNextPageToken();
            }
            $response = $calendarService->events->listEvents('primary', $queryParams);
            $items = array_merge($items, $response->getItems());
            $nextPage = $response->getNextPageToken() !== null;
        }
        $items = array_filter($items, function (Event $item) {
           return $item->getStart() !== null && $item->getEnd() !== null;
        });
        $results = [];
        foreach ($items as $item) {
            $results[] = $this->createCalendarEventFromEventItem($item);
        }

        return $results;
    }
    
    private function createCalendarService(CredentialsInterface $credentials): Calendar
    {
        return $this->calendarServiceFactory->createCalendarService($credentials->getGoogleCredential());
    }
    
    private function createCalendarEventFromEventItem(Event $event): CalendarEventInterface
    {
        $attendes = [];
        foreach ($event->getAttendees() as $attendee) {
            $attendes[] = $attendee->getEmail();
        }
        $start = $event->getStart()->getDateTime() === null ? \DateTime::createFromFormat('Y-m-d', $event->getStart()->getDate()) : \DateTime::createFromFormat(\DateTimeInterface::ISO8601, $event->getStart()->getDateTime());
        $end = $event->getEnd()->getDateTime() === null ? \DateTime::createFromFormat('Y-m-d', $event->getEnd()->getDate()) : \DateTime::createFromFormat(\DateTimeInterface::ISO8601, $event->getEnd()->getDateTime());

        return new CalendarEvent(
            $event->getSummary() === null ? '' : $event->getSummary(),
            $event->getLocation() === null ? '' : $event->getLocation(),
            $event->getDescription() === null ? '' : $event->getDescription(),
            $start,
            $end,
            $attendes
        );
    }
}