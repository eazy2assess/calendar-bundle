<?php

namespace Eazy\Bundle\EazyCalendar\Manager;

use Eazy\Bundle\EazyCalendar\Model\CalendarEventInterface;
use Eazy\Bundle\EazyCalendar\Model\CredentialsInterface;
use Eazy\Bundle\EazyCalendar\Model\ListQueryParams;

interface CalendarEventManager
{
    public function addEventToCalendar(CalendarEventInterface $calendarEvent, CredentialsInterface $credentials): void;
    
    public function deleteEventFromCalendar($id, CredentialsInterface $credentials): void;

    /**
     * @param ListQueryParams $listQueryParams
     * @param CredentialsInterface $credentials
     * @return CalendarEventInterface[]
     */
    public function getEventsList(ListQueryParams $listQueryParams, CredentialsInterface $credentials): array;
}