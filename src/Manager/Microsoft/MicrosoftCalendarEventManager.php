<?php

namespace Eazy\Bundle\EazyCalendar\Manager\Microsoft;

use Eazy\Bundle\EazyCalendar\Event\Microsoft\MicrosoftCalendarEventCreatedEvent;
use Eazy\Bundle\EazyCalendar\Factory\Microsoft\MicrosoftGraphFactory;
use Eazy\Bundle\EazyCalendar\Manager\CalendarEventManager;
use Eazy\Bundle\EazyCalendar\Model\CalendarEvent;
use Eazy\Bundle\EazyCalendar\Model\CalendarEventInterface;
use Eazy\Bundle\EazyCalendar\Model\CredentialsInterface;
use Eazy\Bundle\EazyCalendar\Model\ListQueryParams;
use Eazy\Bundle\EazyCalendar\Model\Microsoft\MicrosoftCredential;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class MicrosoftCalendarEventManager implements CalendarEventManager
{
    const CALENDAR_EVENTS_POST_PATH = '/me/events';
    
    private MicrosoftGraphFactory $graphFactory;
    
    private EventDispatcherInterface $eventDispatcher;
    
    public function __construct(MicrosoftGraphFactory $graphFactory, EventDispatcherInterface $eventDispatcher)
    {
        $this->graphFactory = $graphFactory;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function addEventToCalendar(CalendarEventInterface $calendarEvent, CredentialsInterface $credentials): void
    {
        $attendees = [];
        foreach ($calendarEvent->getAttendees() as $attendee) {
            $attendees[] = ['emailAddress' => ['address' => $attendee]];
        }
        $event = [
            'subject' => $calendarEvent->getTitle(),
            'body' => [
                'content' => $calendarEvent->getDescription(),
                'contentType' => 'text',
            ],
            'start' => [
                'dateTime' => $calendarEvent->getStartDate()->format(\DateTimeInterface::ISO8601),
                'timeZone' => $calendarEvent->getStartDate()->getTimezone()->getName(),
            ],
            'end' => [
                'dateTime' => $calendarEvent->getEndDate()->format(\DateTimeInterface::ISO8601),
                'timeZone' => $calendarEvent->getEndDate()->getTimezone()->getName(),
            ],
            'attendees' => $attendees
        ];
        
        $response = $this->createGraph($credentials)
            ->createRequest(Request::METHOD_POST, self::CALENDAR_EVENTS_POST_PATH)
            ->attachBody($event)
            ->setReturnType(Event::class)
            ->execute();
        
        $this->eventDispatcher->dispatch(new MicrosoftCalendarEventCreatedEvent($response, $calendarEvent->getAdditionalData()));
    }

    public function deleteEventFromCalendar($id, CredentialsInterface $credentials): void
    {
        // TODO: Implement deleteEventFromCalendar() method.
    }

    public function getEventsList(ListQueryParams $listQueryParams, CredentialsInterface $credentials): array
    {
        $query = [
            '$orderby' => 'start/dateTime',
            '$top' => $listQueryParams->getMaxResults(),
            '$select' => 'subject,organizer,attendees,start,end,body,bodyPreview'
        ];
        if ($listQueryParams->getFrom() instanceof \DateTime) {
            $query['StartDateTime'] = $listQueryParams->getFrom()->format(\DateTimeInterface::ISO8601);
        }
        if ($listQueryParams->getTo() instanceof \DateTime) {
            $query['EndDateTime'] = $listQueryParams->getTo()->format(\DateTimeInterface::ISO8601);
        }

        $graphQuery = $this->createGraph($credentials)->createRequest('GET', '/me/calendarView?'.http_build_query($query));
        if ($listQueryParams->getTimezone() instanceof \DateTimeZone) {
            $graphQuery->addHeaders([
                'Prefer' => 'outlook.timezone="'.$listQueryParams->getTimezone()->getName().'"'
            ]);
        }
        $events = $graphQuery->setReturnType(Event::class)->execute();

        $results = [];
        foreach ($events as $event) {
            $results[] = $this->createCalendarEventFromEventItem($event);
        }
    
        return $results;
    }
    
    private function createGraph(CredentialsInterface $credentials): Graph
    {
        return $this->graphFactory->create($credentials->getMicrosoftCredential());
    }

    private function createCalendarEventFromEventItem(Event $event): CalendarEventInterface
    {
        $attendes = [];
        
        foreach ($event->getAttendees() as $attendee) {
            if (!isset($attendee['emailAddress'])) {
                continue;
            }
            $attendes[] = $attendee['emailAddress'];
        }
        $start = \DateTime::createFromFormat('Y-m-d\TH:i:s', explode('.', $event->getStart()->getDateTime())[0]);
        $end = \DateTime::createFromFormat('Y-m-d\TH:i:s', explode('.', $event->getEnd()->getDateTime())[0]);

        return new CalendarEvent(
            $event->getSubject() === null ? '' : $event->getSubject(),
            $event->getLocation() === null ? '' : $event->getLocation(),
            $event->getBodyPreview() === null ? '' : $event->getBodyPreview(),
            $start,
            $end,
            $attendes
        );
    } 
}