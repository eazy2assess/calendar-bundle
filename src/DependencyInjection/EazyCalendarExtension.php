<?php

namespace Eazy\Bundle\EazyCalendar\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class EazyCalendarExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('eazy_calendar.google.authorization.client_id', $config['google']['authorization']['client_id']);
        $container->setParameter('eazy_calendar.google.authorization.project_id', $config['google']['authorization']['project_id']);
        $container->setParameter('eazy_calendar.google.authorization.auth_uri', $config['google']['authorization']['auth_uri']);
        $container->setParameter('eazy_calendar.google.authorization.token_uri', $config['google']['authorization']['token_uri']);
        $container->setParameter('eazy_calendar.google.authorization.auth_provider_x509_cert_url', $config['google']['authorization']['auth_provider_x509_cert_url']);
        $container->setParameter('eazy_calendar.google.authorization.client_secret', $config['google']['authorization']['client_secret']);
        $container->setParameter('eazy_calendar.google.authorization.redirect_uri', $config['google']['authorization']['redirect_uri']);
        $container->setParameter('eazy_calendar.google.authorization.javascript_origins', $config['google']['authorization']['javascript_origins']);
        $container->setParameter('eazy_calendar.google.routing.get_auth_uri', $config['google']['routing']['get_auth_uri']);
        $container->setParameter('eazy_calendar.google.routing.token_receive', $config['google']['routing']['token_receive']);

        $container->setParameter('eazy_calendar.microsoft.routing.get_auth_uri', $config['microsoft']['routing']['get_auth_uri']);
        $container->setParameter('eazy_calendar.microsoft.routing.token_receive', $config['microsoft']['routing']['token_receive']);
        $container->setParameter('eazy_calendar.microsoft.authorization.client_id', $config['microsoft']['authorization']['client_id']);
        $container->setParameter('eazy_calendar.microsoft.authorization.client_secret', $config['microsoft']['authorization']['client_secret']);
        $container->setParameter('eazy_calendar.microsoft.authorization.redirect_uri', $config['microsoft']['authorization']['redirect_uri']);
        $container->setParameter('eazy_calendar.microsoft.authorization.url_authorize', $config['microsoft']['authorization']['url_authorize']);
        $container->setParameter('eazy_calendar.microsoft.authorization.url_access_token', $config['microsoft']['authorization']['url_access_token']);
        $container->setParameter('eazy_calendar.microsoft.authorization.url_resource_owner_details', $config['microsoft']['authorization']['url_resource_owner_details']);
        $container->setParameter('eazy_calendar.microsoft.authorization.scopes', $config['microsoft']['authorization']['scopes']);


        $fileLocator = new FileLocator(__DIR__ . '/../Resources/config');
        
        $yamlLoader  = new YamlFileLoader($container, $fileLocator);
        $yamlLoader->load('services.yaml');
        if ($config['use_generic_calendar_event']) {
            if (!interface_exists(\Symfony\Component\Messenger\MessageBusInterface::class)) {
                throw new \Exception('To use generic events install messenger bundle');
            }
            $yamlLoader->load('generic_event.yaml');
        }
    }
}