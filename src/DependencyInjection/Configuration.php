<?php

namespace Eazy\Bundle\EazyCalendar\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('eazy_calendar');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->booleanNode('use_generic_calendar_event')->defaultValue(false)->end()
                ->arrayNode('microsoft')
                    ->children()
                        ->arrayNode('routing')
                            ->children()
                                ->scalarNode('token_receive')->defaultValue('/calendar/microsoft/token')->isRequired()->end()
                                ->scalarNode('get_auth_uri')->defaultValue('/calendar/microsoft/auth_uri')->isRequired()->end()
                        ->end()->end()
                        ->arrayNode('authorization')
                            ->children()
                                ->scalarNode('client_id')->isRequired()->end()
                                ->scalarNode('client_secret')->isRequired()->end()
                                ->scalarNode('redirect_uri')->isRequired()->end()
                                ->scalarNode('url_authorize')->defaultValue('https://login.microsoftonline.com/common/oauth2/v2.0/authorize')->isRequired()->end()
                                ->scalarNode('url_access_token')->defaultValue('https://login.microsoftonline.com/common/oauth2/v2.0/token')->isRequired()->end()
                                ->scalarNode('url_resource_owner_details')->defaultValue('')->isRequired()->end()
                                ->scalarNode('scopes')->defaultValue('openid profile offline_access user.read calendars.readwrite')->isRequired()->end()
                            ->end()->end()->end()->end()
                ->arrayNode('google')
                    ->children()
                        ->arrayNode('routing')
                            ->children()
                                ->scalarNode('token_receive')->defaultValue('/calendar/google/token')->isRequired()->end()
                                ->scalarNode('get_auth_uri')->defaultValue('/calendar/google/auth_uri')->isRequired()->end()
                        ->end()->end()
                        ->arrayNode('authorization')
                            ->children()
                                ->scalarNode('client_id')->isRequired()->end()
                                ->scalarNode('project_id')->isRequired()->end()
                                ->scalarNode('auth_uri')->defaultValue('https://accounts.google.com/o/oauth2/auth')->isRequired()->end()
                                ->scalarNode('token_uri')->defaultValue('https://oauth2.googleapis.com/token')->isRequired()->end()
                                ->scalarNode('auth_provider_x509_cert_url')->defaultValue('https://www.googleapis.com/oauth2/v1/certs')->isRequired()->end()
                                ->scalarNode('client_secret')->isRequired()->end()
                                ->scalarNode('redirect_uri')->isRequired()->end()
                                ->arrayNode('javascript_origins')->prototype('scalar')->isRequired()->end()
                        ->end()
            ->end();

        return $treeBuilder;
    }
}