<?php

namespace Eazy\Bundle\EazyCalendar\EventSubscriber;

use Eazy\Bundle\EazyCalendar\Event\DeleteCalendarEvent;
use Eazy\Bundle\EazyCalendar\Event\NewCalendarEvent;
use Eazy\Bundle\EazyCalendar\Message\Google\AddNewGoogleCalendarEventMessage;
use Eazy\Bundle\EazyCalendar\Message\Google\DeleteGoogleCalendarEventMessage;
use Eazy\Bundle\EazyCalendar\Message\Microsoft\AddNewMicrosoftCalendarEventMessage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class CalendarEventSubscriber implements EventSubscriberInterface
{
    private MessageBusInterface $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public function addGoogleCalendarEvent(NewCalendarEvent $event): void
    {
        if ($event->getCalenderCredentials()->getGoogleCredential() === null) {
            return;
        }
        
        $this->bus->dispatch(new AddNewGoogleCalendarEventMessage($event));
    }
    
    public function deleteGoogleCalendarEvent(DeleteCalendarEvent $event): void
    {
        if ($event->getCredentials()->getGoogleCredential() === null) {
            return;
        }
        if ($event->getSource() !== DeleteCalendarEvent::GOOGLE_SOURCE) {
            return;
        }
        
        $this->bus->dispatch(new DeleteGoogleCalendarEventMessage($event->getId(), $event->getCredentials()));
    }

    public function addMicrosoftCalendarEvent(NewCalendarEvent $event): void
    {
        if ($event->getCalenderCredentials()->getMicrosoftCredential() === null) {
            return;
        }

        $this->bus->dispatch(new AddNewMicrosoftCalendarEventMessage($event));
    }
    
    public static function getSubscribedEvents()
    {
        return [
            NewCalendarEvent::class => [
                ['addGoogleCalendarEvent'],
                ['addMicrosoftCalendarEvent']
            ],
            DeleteCalendarEvent::class => [
                ['deleteGoogleCalendarEvent']
            ]
        ];
    }
}