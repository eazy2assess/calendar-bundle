<?php

namespace Eazy\Bundle\EazyCalendar\Event\Google;

use Symfony\Contracts\EventDispatcher\Event;
use Google\Service\Calendar\Event as GoogleCalendarEvent;

class GoogleCalendarEventCreatedEvent extends Event
{
    private GoogleCalendarEvent $event;
    
    private array $additionalData = [];
    
    public function __construct(GoogleCalendarEvent $event, array $additionalData = [])
    {
        $this->event = $event;
        $this->additionalData = $additionalData;
    }

    public function getEvent(): GoogleCalendarEvent
    {
        return $this->event;
    }

    public function getAdditionalData(): array
    {
        return $this->additionalData;
    }
}