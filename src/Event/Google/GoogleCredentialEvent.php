<?php

namespace Eazy\Bundle\EazyCalendar\Event\Google;

use Eazy\Bundle\EazyCalendar\Model\Google\GoogleCredential;
use Symfony\Contracts\EventDispatcher\Event;

abstract class GoogleCredentialEvent extends Event
{
    private GoogleCredential $credential;

    private array $aditionalData = [];
    
    public function __construct(GoogleCredential $credential, array $aditionalData = [])
    {
        $this->credential = $credential;
        $this->aditionalData = $aditionalData;
    }

    public function getCredential(): GoogleCredential
    {
        return $this->credential;
    }

    public function getAditionalData(): array
    {
        return $this->aditionalData;
    }
}