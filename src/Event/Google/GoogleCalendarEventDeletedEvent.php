<?php

namespace Eazy\Bundle\EazyCalendar\Event\Google;

use Symfony\Contracts\EventDispatcher\Event;

class GoogleCalendarEventDeletedEvent extends Event
{
    private $id;
    
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }
}