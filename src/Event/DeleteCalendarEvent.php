<?php

namespace Eazy\Bundle\EazyCalendar\Event;

use Eazy\Bundle\EazyCalendar\Model\CredentialsInterface;

class DeleteCalendarEvent
{
    const GOOGLE_SOURCE = 'google';
    const MICROSOFT_SOURCE = 'microsoft';
    
    private $id;
    
    private string $source;
    
    private CredentialsInterface $credentials;

    public function __construct($id, string $source, CredentialsInterface $credentials)
    {
        $this->id = $id;
        $this->source = $source;
        $this->credentials = $credentials;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getCredentials(): CredentialsInterface
    {
        return $this->credentials;
    }
}