<?php

namespace Eazy\Bundle\EazyCalendar\Event\Microsoft;

use Eazy\Bundle\EazyCalendar\Model\Microsoft\MicrosoftCredential;
use Symfony\Contracts\EventDispatcher\Event;

abstract class MicrosoftCredentialEvent extends Event
{
    private MicrosoftCredential $credential;

    private array $aditionalData = [];

    public function __construct(MicrosoftCredential $credential, array $aditionalData = [])
    {
        $this->credential = $credential;
        $this->aditionalData = $aditionalData;
    }

    public function getCredential(): MicrosoftCredential
    {
        return $this->credential;
    }

    public function getAditionalData(): array
    {
        return $this->aditionalData;
    }
}