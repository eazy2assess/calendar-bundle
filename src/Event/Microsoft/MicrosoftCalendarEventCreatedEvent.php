<?php

namespace Eazy\Bundle\EazyCalendar\Event\Microsoft;

use Symfony\Contracts\EventDispatcher\Event;
use Microsoft\Graph\Model\Event as MicrosoftCalendarEvent;

class MicrosoftCalendarEventCreatedEvent extends Event 
{
    private MicrosoftCalendarEvent $event; 
    
    private array $additionalData = [];

    public function __construct(MicrosoftCalendarEvent $event, array $additionalData = [])
    {
        $this->event = $event;
        $this->additionalData = $additionalData;
    }

    public function getEvent(): MicrosoftCalendarEvent
    {
        return $this->event;
    }

    public function getAdditionalData(): array
    {
        return $this->additionalData;
    }
}