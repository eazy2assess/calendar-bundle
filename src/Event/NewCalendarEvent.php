<?php

namespace Eazy\Bundle\EazyCalendar\Event;

use Eazy\Bundle\EazyCalendar\Model\CalendarEventInterface;
use Eazy\Bundle\EazyCalendar\Model\CredentialsInterface;

class NewCalendarEvent
{
    private CalendarEventInterface $calendarEvent;

    private CredentialsInterface $credentials;
    
    public function __construct(CalendarEventInterface $calendarEvent, CredentialsInterface $credentials)
    {
        $this->calendarEvent = $calendarEvent;
        $this->credentials = $credentials;
    }

    public function getCalendarEvent(): CalendarEventInterface
    {
        return $this->calendarEvent;
    }

    public function getCalenderCredentials(): CredentialsInterface
    {
        return $this->credentials;
    }
}