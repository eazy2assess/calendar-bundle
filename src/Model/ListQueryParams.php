<?php

namespace Eazy\Bundle\EazyCalendar\Model;

class ListQueryParams
{
    private ?\DateTime $from;
    
    private ?\DateTime $to;
    
    private int $maxResults;
    
    private ?\DateTimeZone $timezone;
    
    public function __construct(
        int $maxResults = 250,
        \DateTime $from = null,
        \DateTime $to = null,
        ?\DateTimeZone $timeZone = null
    )
    {
        $this->from = $from;
        $this->to = $to;
        $this->maxResults = $maxResults;
        $this->timezone = $timeZone;
    }

    public function getFrom(): ?\DateTime
    {
        return $this->from;
    }

    public function getTo(): ?\DateTime
    {
        return $this->to;
    }

    public function getMaxResults(): int
    {
        return $this->maxResults;
    }

    /**
     * @param \DateTime|null $from
     */
    public function setFrom(?\DateTime $from): void
    {
        $this->from = $from;
    }

    /**
     * @param \DateTime|null $to
     */
    public function setTo(?\DateTime $to): void
    {
        $this->to = $to;
    }

    /**
     * @return \DateTimeZone|null
     */
    public function getTimezone(): ?\DateTimeZone
    {
        return $this->timezone;
    }

    /**
     * @param \DateTimeZone|null $timezone
     */
    public function setTimezone(?\DateTimeZone $timezone): void
    {
        $this->timezone = $timezone;
    }
}