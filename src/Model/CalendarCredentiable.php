<?php

namespace Eazy\Bundle\EazyCalendar\Model;

use Eazy\Bundle\EazyCalendar\Model\Google\GoogleCredential;

interface CalendarCredentiable
{
    public function getCalendarCredentials(): CredentialsInterface;
}