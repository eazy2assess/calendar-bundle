<?php

namespace Eazy\Bundle\EazyCalendar\Model;

interface CalendarEventInterface
{
    public function getTitle(): string;

    public function getLocation(): ?string;

    public function getDescription(): string;

    public function getStartDate(): \DateTimeInterface;

    public function getEndDate(): \DateTimeInterface;

    public function getAttendees(): array;
    
    public function getAdditionalData(): array;
}