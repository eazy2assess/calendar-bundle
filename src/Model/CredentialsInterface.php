<?php

namespace Eazy\Bundle\EazyCalendar\Model;

use Eazy\Bundle\EazyCalendar\Model\Google\GoogleCredential;
use Eazy\Bundle\EazyCalendar\Model\Microsoft\MicrosoftCredential;

interface CredentialsInterface
{
    const GOOGLE_CALENDAR = 'google';
    const MICROSOFT_CALENDAR = 'microsoft';
    
    public function getGoogleCredential(): ?GoogleCredential;
    
    public function getMicrosoftCredential(): ?MicrosoftCredential;
    
    public function isEmpty(): bool;
}