<?php

namespace Eazy\Bundle\EazyCalendar\Model;

class CalendarEvent implements CalendarEventInterface
{
    private string $title;

    private ?string $location;

    private string $description;

    private \DateTimeInterface $startDate;

    private \DateTimeInterface $endDate;

    private array $attendees = [];
    
    private array $additionalData = [];
    
    public function __construct(
        string $title,
        ?string $location,
        string $description,
        \DateTimeInterface $startDate,
        \DateTimeInterface $endDate,
        array $attendees,
        array $additionalData = []
    )
    {
        $this->title = $title;
        $this->location = $location;
        $this->description = $description;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->attendees = $attendees;
        $this->additionalData = $additionalData;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    public function getEndDate(): \DateTimeInterface
    {
        return $this->endDate;
    }

    public function getAttendees(): array
    {
        return $this->attendees;
    }

    public function getAdditionalData(): array
    {
        return $this->additionalData;
    }
}