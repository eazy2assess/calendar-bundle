<?php

namespace Eazy\Bundle\EazyCalendar\Model\Microsoft;

class MicrosoftCredential
{
    private int $createdAt;
    
    private string $accessToken;
    
    private int $expires;
    
    private string $refreshToken;
    
    private array $values;

    private ?string $ownerIdentifier;
    
    public function __construct(int $createdAt, string $accessToken, int $expires, string $refreshToken, array $values, ?string $ownerIdentifier = null)
    {
        $this->createdAt = $createdAt;
        $this->accessToken = $accessToken;
        $this->expires = $expires;
        $this->refreshToken = $refreshToken;
        $this->values = $values;
        $this->ownerIdentifier = $ownerIdentifier;
    }

    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function getExpires(): int
    {
        return $this->expires;
    }

    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function getOwnerIdentifier(): ?string
    {
        return $this->ownerIdentifier;
    }

    public function setCreatedAt(int $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    public function setExpires(int $expires): void
    {
        $this->expires = $expires;
    }

    public function setRefreshToken(string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    public function setValues(array $values): void
    {
        $this->values = $values;
    }

    public function setOwnerIdentifier(?string $ownerIdentifier): void
    {
        $this->ownerIdentifier = $ownerIdentifier;
    }
}