<?php

namespace Eazy\Bundle\EazyCalendar\Model;

use Eazy\Bundle\EazyCalendar\Model\Google\GoogleCredential;
use Eazy\Bundle\EazyCalendar\Model\Microsoft\MicrosoftCredential;

class Credentials implements CredentialsInterface
{
    private array $credentials = [];
    
    public function setGoogleCredentials(GoogleCredential $googleCredential): void
    {
        $this->credentials[self::GOOGLE_CALENDAR] = $googleCredential;
    }

    public function getGoogleCredential(): ?GoogleCredential
    {
        return $this->credentials[self::GOOGLE_CALENDAR] ?? null;
    }

    public function getMicrosoftCredential(): ?MicrosoftCredential
    {
        return $this->credentials[self::MICROSOFT_CALENDAR] ?? null;
    }

    public function setMicrosoftCredentials(MicrosoftCredential $microsoftCredential): void
    {
        $this->credentials[self::MICROSOFT_CALENDAR] = $microsoftCredential;
    }
    
    public function isEmpty(): bool
    {
        return empty($this->credentials);
    }
}