<?php

namespace Eazy\Bundle\EazyCalendar\Model\Google;

class GoogleCredential
{
    private string $token;

    private int $expiresIn;

    private int $createdAt;

    private ?string $refreshToken;
    
    private ?string $ownerIdentifier;
    
    public function __construct(
        string $token,
        int $expiresIn,
        int $createdAt,
        ?string $refreshToken,
        ?string $ownerIdentifier = null
    )
    {
        $this->token = $token;
        $this->expiresIn = $expiresIn;
        $this->createdAt = $createdAt;
        $this->refreshToken = $refreshToken;
        $this->ownerIdentifier = $ownerIdentifier;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function setRefreshToken(?string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    public function getExpiresIn(): int
    {
        return $this->expiresIn;
    }

    public function setExpiresIn(int $expiresIn): void
    {
        $this->expiresIn = $expiresIn;
    }

    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    public function setCreatedAt(int $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getOwnerIdentifier(): ?string
    {
        return $this->ownerIdentifier;
    }
}