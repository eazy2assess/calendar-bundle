<?php

namespace Eazy\Bundle\EazyCalendar\MessageHandler\Microsoft;

use Eazy\Bundle\EazyCalendar\Manager\Google\GoogleCalendarEventsManager;
use Eazy\Bundle\EazyCalendar\Message\Microsoft\AddNewMicrosoftCalendarEventMessage;

class AddNewMicrosoftCalendarEventMessageHandler extends MicrosoftCalendarEventMessageHandler
{
    public function __invoke(AddNewMicrosoftCalendarEventMessage $message)
    {
        $message->getCalendarEvent()->getCalenderCredentials()->getMicrosoftCredential()->setOwnerIdentifier('59470513-874c-4dfb-a821-efc5dccfb37c');

        $this->getMicrosoftCalendarEventsManager()->addEventToCalendar(
            $message->getCalendarEvent()->getCalendarEvent(),
            $message->getCalendarEvent()->getCalenderCredentials()
        );
    }
}