<?php

namespace Eazy\Bundle\EazyCalendar\MessageHandler\Microsoft;

use Eazy\Bundle\EazyCalendar\Manager\Microsoft\MicrosoftCalendarEventManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

abstract class MicrosoftCalendarEventMessageHandler implements MessageHandlerInterface
{
    private MicrosoftCalendarEventManager $calendarEventManager;

    public function __construct(MicrosoftCalendarEventManager $manager)
    {
        $this->calendarEventManager = $manager;
    }
    
    protected function getMicrosoftCalendarEventsManager(): MicrosoftCalendarEventManager
    {
        return $this->calendarEventManager;
    }
}