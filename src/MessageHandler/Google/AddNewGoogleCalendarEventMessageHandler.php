<?php

namespace Eazy\Bundle\EazyCalendar\MessageHandler\Google;

use Eazy\Bundle\EazyCalendar\Manager\Google\GoogleCalendarEventsManager;
use Eazy\Bundle\EazyCalendar\Message\Google\AddNewGoogleCalendarEventMessage;

class AddNewGoogleCalendarEventMessageHandler extends GoogleCalendarEventMessageHandler
{
    public function __invoke(AddNewGoogleCalendarEventMessage $message)
    {
        $this->getGoogleCalendarEventsManager()->addEventToCalendar(
            $message->getCalendarEvent()->getCalendarEvent(),
            $message->getCalendarEvent()->getCalenderCredentials()
        );
    }
}