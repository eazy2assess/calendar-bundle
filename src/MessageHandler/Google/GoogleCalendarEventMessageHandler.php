<?php

namespace Eazy\Bundle\EazyCalendar\MessageHandler\Google;

use Eazy\Bundle\EazyCalendar\Manager\Google\GoogleCalendarEventsManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

abstract class GoogleCalendarEventMessageHandler implements MessageHandlerInterface
{
    private GoogleCalendarEventsManager $calendarEventManager;

    public function __construct(GoogleCalendarEventsManager $manager)
    {
        $this->calendarEventManager = $manager;
    }
    
    protected function getGoogleCalendarEventsManager(): GoogleCalendarEventsManager
    {
        return $this->calendarEventManager;
    }
}