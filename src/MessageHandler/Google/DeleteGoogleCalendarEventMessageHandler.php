<?php

namespace Eazy\Bundle\EazyCalendar\MessageHandler\Google;

use Eazy\Bundle\EazyCalendar\Message\Google\DeleteGoogleCalendarEventMessage;

class DeleteGoogleCalendarEventMessageHandler extends GoogleCalendarEventMessageHandler
{
    public function __invoke(DeleteGoogleCalendarEventMessage $message)
    {
        $this->getGoogleCalendarEventsManager()->deleteEventFromCalendar(
            $message->getId(),
            $message->getCredentials()
        );
    }
}