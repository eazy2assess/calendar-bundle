<?php

namespace Eazy\Bundle\EazyCalendar\Controller\Microsoft;

use Eazy\Bundle\EazyCalendar\Event\Microsoft\MicrosoftTokenReceivedEvent;
use Eazy\Bundle\EazyCalendar\Service\Microsoft\MicrosoftAuthService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class MicrosoftAuthController extends AbstractController
{
    private MicrosoftAuthService $authService;
    
    private EventDispatcherInterface $eventDispatcher;
    
    public function __construct(MicrosoftAuthService $authService, EventDispatcherInterface $eventDispatcher)
    {
        $this->authService = $authService;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getAuthUri(): Response
    {
        return $this->json(['uri' => $this->authService->getAuthUri()]);
    }

    public function receiveToken(Request $request): Response
    {
        $content = json_decode($request->getContent(), true);
        if (!isset($content['token']) || empty($content['token']) || !is_string($content['token'])) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Missing token');
        }
        $credential = $this->authService->getAuthorizationDataByCode($content['token']);
        $this->eventDispatcher->dispatch(new MicrosoftTokenReceivedEvent($credential));

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}