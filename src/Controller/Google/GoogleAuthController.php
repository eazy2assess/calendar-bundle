<?php

namespace Eazy\Bundle\EazyCalendar\Controller\Google;

use Eazy\Bundle\EazyCalendar\Event\Google\GoogleTokenReceivedEvent;
use Eazy\Bundle\EazyCalendar\Manager\Google\GoogleCalendarEventsManager;
use Eazy\Bundle\EazyCalendar\Service\Google\GoogleAuthService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class GoogleAuthController extends AbstractController
{
    private EventDispatcherInterface $eventDispatcher;

    private GoogleAuthService $authService;

    public function __construct(EventDispatcherInterface $eventDispatcher, GoogleAuthService $authService)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->authService = $authService;
    }

    public function getAuthUri(): Response
    {
        $authUrl = $this->authService->getAuthUri();

        return $this->json(['auth_url' => $authUrl]);
    }

    public function receiveToken(Request $request): Response
    {
        $content = json_decode($request->getContent(), true);
        if (!isset($content['token']) || empty($content['token']) || !is_string($content['token'])) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Missing token');
        }
        $credential = $this->authService->getAuthorizationDataByCode($content['token']);
        $this->eventDispatcher->dispatch(new GoogleTokenReceivedEvent($credential));

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}